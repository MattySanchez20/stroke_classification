# Stroke Classification

## Standardisation

![before](before_process.png)

Standardisation was applicable for bmi, age and glucose level because they were, at most, 2 orders of magnitude apart which could bias the model. The distributions roughly followed a normal distribution so normalisation didn't seem to be necessary. The figure above shows the distributions before standardisation. The figure below shows after standardisation.

![after](after_process.png)

## Oversampling and Undersampling

![bef_sampling](before_oversampling.png)

Looking at the number of patients who suffered a stroke versus the ones who did not, a form of sampling had to be applied. The number of stroke victims was approx. 200 whereas the number of patients who did not suffer strokes was more than 4000. It seemed more appropriate to undersample because 250 points would not produce a very representative distribution to generate new points from. However, both undersampling and oversampling were tried. The figure above shows the distribution before any form of sampling was applied.

#### Oversampling:

![aft_oversampling](after_oversampling.png)

#### Undersampling:

![aft_undersampling](after_undersampling.png)

## The Models

The first model used was a KNN which served as a baseline for the more complicated models. The model was trained using multiple values of K to create the elbow graph. The value of K was set to 20 because any higher would lead to more computing power and very little increment in accuracy. The elbow graph can be seen below.

![elbow](elbow_graph.png)

## Results

#### KNN
The baseline KNN model results can be seen in the classification report below:

![KNN_report](KNN_classification_report.PNG)

#### Decision Tree:
The results for oversampling are shown in the matrix below:

![deetree_over](oversampling_decision_tree_classification_report.PNG)

The results for undersampling are shown in the matrix below:

![deetree_under](undersampling_decision_tree_classification_report.PNG)

#### Random Forest
The results for oversampling are shown in the matrix below:

![rantree_over](oversampling_random_forest_classification_report.PNG)

The results for underrsampling are shown in the matrix below:

![rantree_under](undersampling_random_forest_classification_report.PNG)

## Reflection

Looking back at the project, I followed a sound process which entailed using a baseline model to provide a reality check for the other complicated models. Oversampling seemed to be inappropriate for this dataset because there were too few points in the minority class to create a representative distribution for point generation. This was evidenced by the >98% accuracy for the RandomForest and DecisionTree model. When using undersampling, the accuracy seemed far more realistic. 

For the next classification model, I will implement normalisation to distributions that do not follow a normal distribution. Looking back to when I did this project, I should have implemented normalisation to the average glucose level since it showed more of a bi-modal distribution.
